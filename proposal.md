![Screenshot of the HANDY population dynamic model](https://gitlab.com/adamwight/36c3.awight/raw/master/handy-initial.png)

Summary
---
Algorithms bear the image of their makers.  Three algorithmic systems are
revealed to have embodied class interests.  First, a population ecology modeled
by two predator-prey equations leads its reader to conclude that socialist
revolution and compulsory leisure are the only routes to avoiding civilization
collapse.  Second, a formula for labor supply reduces us to lazy drones, who
work as little as possible to support our lifestyle.  Finally, advertising on
Wikipedia could be a multi-billion-dollar revenue stream—shall we put it up for
sale or learn to share?

Description
---
(1) The [Human and Nature Dynamics](https://www.sciencedirect.com/science/article/pii/S0921800914000615)
(HANDY) model is the first to pair environmental resource consumption with
class conflict, as two predator-prey cycles.  In one cycle we overrun a fragile and
defenseless Earth, and in the other elites outcompete commoners in consumption,
even to the point of causing them famine.  One can say that socialist
revolution is embedded in a statement like this.  Indeed, something must be
done about the growth of over-consuming elites before they doom us all.  I will
give a tour using this [interactive explorer](https://adamwight.github.io/handy-explorer/).

(2) A second example is the typical, capitalist formula for labor supply, or
"whether you go to work in the morning", loosely `optimize(Consumption, hours
worked)` for the constraint `Consumption ≤ wage x hours + entitlement`.  In
other words, we're seen as lazy and greedy individual agents, each wanting the
greatest comfort for the least labor.  The worker who internalizes this formula
will fight for fewer hours of work and higher wages for themself, will find
shortcuts to spend less money, and can be expected to vote in favor of social
democratic minimum incomes.  A company following this formula, on the other
hand, will fight against all of these worker gains, and will act to depress
government welfare or minimum incomes until workers are on the edge of
starvation in order to squeeze longer hours out of them.  What's missing from
the formula are mutual aid to protect the most vulnerable individuals, pooling
resources, and any other motivation to work besides mortal fear and hedonism.
—One can easily imagine a radically different paradigm for work, in which labor
is dignified and fulfilling.  To understand this world in formulas, labor
supply is measured in education levels, self-direction, and other positive
feedback loops which raise productivity.

(3) Wikipedia and its sister projects have never worn the shackles of paid
advertising, although they sit on a potential fountain of revenue in the tens
of billions of dollars per year—not to mention the value of the influence over
public opinion that such a propaganda machine might achieve.  `Revenue = Ads
per visit x Visits`  Analyzed venally, Wikipedia becomes an appealing portfolio
acquisition, which would jeopardize the entire free-open movement.  From a
different perspective, that of an organizer in an editor’s association, slicing
pageview and (non)-advertising data might allow for more effective
resource-sharing among the many chapter organizations.  In a third analysis
using a flow of labor, power, and funds, we can see the Wikimedia Foundation as
engaged in illegitimate expropriation, turning editors into sharecroppers and
suppressing decentralized growth.  These twists all come about through
variations on an equation.  Which shall we choose?
